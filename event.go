package cchat

import (
	"bytes"
	"encoding/json"
	"log"
	"strconv"

	"github.com/isqad/melody"
)

// EventType is type of Websocket event
type EventType string

const (
	// LocalSDPEvent is event contains SDP from browser (client)
	LocalSDPEvent = "local_sdp"
	// StartStreamEvent is event contains SDP from server and stream data
	StartStreamEvent = "start_stream"
	// PauseStreamEvent is event when user sets pause the stream
	// it appears when user want to stop broadcasting
	// when come this event, we should disable sending any RTP packets to viewers,
	// send event about pause to all viewers
	PauseStreamEvent = "pause_stream"
	// MessageEvent is event represents chat message from user
	MessageEvent = "message"
)

// WsEvent is object of Websocket events
type WsEvent struct {
	Type    EventType `json:"type"`
	Payload string    `json:"payload"`
}

// HandleWsEvent handles given event from WebSocket
// TODO: inject logger?
func HandleWsEvent(s *melody.Session, msg []byte) {
	streamsHandler := s.MustGet(StreamsHandlerKey).(*StreamsHandler)

	event := &WsEvent{}
	decoder := json.NewDecoder(bytes.NewReader(msg))
	if err := decoder.Decode(event); err != nil {
		log.Printf("Failed to decode ws event: %v\n", err)
		return
	}

	switch event.Type {
	case LocalSDPEvent:
		log.Println("Got SDP event...")
		stream := NewStream(s, event.Payload)
		streamsHandler.Publish <- stream
	case PauseStreamEvent:
		log.Println("Got pause event...")
		streamID, err := strconv.ParseUint(event.Payload, 10, 32)
		if err != nil {
			log.Printf("Failed to decode ws payload of event: %v\n", err)
			return
		}
		streamsHandler.PausePublish <- uint32(streamID)
	case MessageEvent:
		log.Println("Got Message event")
	default:
		log.Println("Unkonwn event type")
	}
}
