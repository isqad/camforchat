module cchat

go 1.14

require (
	github.com/at-wat/ebml-go v0.11.0
	github.com/go-chi/chi v4.1.1+incompatible
	github.com/google/uuid v1.1.1
	github.com/isqad/melody v0.0.0-20200429135154-b03b96e6f3bf
	github.com/pion/rtcp v1.2.1
	github.com/pion/rtp v1.4.0
	github.com/pion/webrtc/v2 v2.2.8
)
