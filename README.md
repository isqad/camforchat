# CamForChat.net

# Installation

Used WebRTC technology. For broadcasting from webcams, an SSL certificate is required.
In a development environment, you need to generate a certificate for localhost:

```
./scripts/generate_ssl_certs.sh
```

then try to full restart Chromium (or other browser), and run

```
./cchat
```

then run https://localhost:8083 in browser.

To view full list of options run in terminal:

```
./cchat -h
```