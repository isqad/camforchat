package cchat

import (
	"context"
	"log"

	"github.com/pion/webrtc/v2"
)

const (
	// StreamsHandlerKey is key for persist StreamsHandler between ws sessions
	StreamsHandlerKey = "streamsHandler"
)

// default STUN and TURN servers
var iceServers = []string{"stun:stun.l.google.com:19302"}

// StreamsHandler handles streams
// streamer creates Stream object and publish it through chan Publish,
// new streamer will be added to Streams map
type StreamsHandler struct {
	Streams map[uint32]*Stream

	Publish      chan *Stream
	PausePublish chan uint32

	// global webrtc API object for interact with webrtc (e.x. creating peerConnection and etc.)
	webrtcAPI *webrtc.API
}

// NewStreamsHandler creates new server for streaming
func NewStreamsHandler() *StreamsHandler {
	m := webrtc.MediaEngine{}
	m.RegisterCodec(webrtc.NewRTPVP8Codec(webrtc.DefaultPayloadTypeVP8, 90000))
	m.RegisterCodec(webrtc.NewRTPOpusCodec(webrtc.DefaultPayloadTypeOpus, 48000))

	return &StreamsHandler{
		Streams: make(map[uint32]*Stream),
		Publish: make(chan *Stream),

		PausePublish: make(chan uint32),
		webrtcAPI:    webrtc.NewAPI(webrtc.WithMediaEngine(m)),
	}
}

// ListenAndAccept listens and accepts new SDP
func (sh *StreamsHandler) ListenAndAccept(ctx context.Context) {
	go func() {
		log.Println("Start StreamsHandler...")

		for {
			select {
			case stream := <-sh.Publish:
				err := stream.Run(sh.webrtcAPI)
				if err != nil {
					log.Printf("Can't run stream: %v", err)
					continue
				}
				sh.Streams[stream.ID] = stream
			case streamID := <-sh.PausePublish:
				stream, ok := sh.Streams[streamID]
				if !ok {
					log.Printf("Pause stream: Stream #%d not found", streamID)
					continue
				}
				stream.Pause()
			case <-ctx.Done():
				sh.Close()
				break
			}
		}
	}()
}

// Close closes all active streams and viewers
func (sh *StreamsHandler) Close() error {
	for _, s := range sh.Streams {
		if err := s.Close(); err != nil {
			log.Printf("Close stream failed: %v", err)
		}

		delete(sh.Streams, s.ID)
	}

	return nil
}
