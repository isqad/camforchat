package main

import (
	"cchat"
	"context"
	"errors"
	"flag"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path"
	"text/template"
	"time"

	"github.com/go-chi/chi"
	"github.com/isqad/melody"
)

const (
	maxWsMessageSize = (20 * 1024) // Max size of incoming message is 20Kib
)

func main() {
	serveAddr := flag.String("addr", "127.0.0.1:8083", "Serve addr for http server")
	tlsCertFile := flag.String("tlsCert", "configs/certs/cert.pem", "Path to tls certificate")
	tlsKeyFile := flag.String("tlsKey", "configs/certs/key.pem", "Path to tls key file")
	flag.Parse()

	quit := make(chan os.Signal, 1)
	done := make(chan bool, 1)

	m := melody.New()
	m.Config.MaxMessageSize = maxWsMessageSize

	r := chi.NewRouter()
	streamsHandler := cchat.NewStreamsHandler()

	// Websockets
	r.Get("/ws", func(w http.ResponseWriter, r *http.Request) {
		sessionContext := make(map[string]interface{})
		sessionContext[cchat.StreamsHandlerKey] = streamsHandler

		m.HandleRequestWithKeys(w, r, sessionContext)
	})
	m.HandleConnect(func(s *melody.Session) {
		// TODO: Identify by session cookie
		log.Println(s.Request.Cookies())
	})
	m.HandleClose(func(s *melody.Session, code int, message string) error {
		// TODO: here we can close peerconnection
		log.Printf("Session closed: %d:%s", code, message)

		return errors.New("Session closed")
	})
	m.HandleMessage(cchat.HandleWsEvent)

	// Homepage
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		tmpl, err := template.New("app").ParseFiles(
			"web/layout.html",
			"web/index.html",
		)
		if err != nil {
			log.Fatal(err)
		}

		tmpl.ExecuteTemplate(w, "layout.html", nil)
	})

	// Serve static assets
	// serves files from web/static dir
	cwd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	staticPrefix := "/static/"
	staticDir := path.Join(cwd, "web", staticPrefix)
	r.Method("GET", staticPrefix+"*", http.StripPrefix(staticPrefix, http.FileServer(http.Dir(staticDir))))

	// Favicon
	r.Get("/favicon.ico", func(w http.ResponseWriter, r *http.Request) {
		if err := serveStaticFile(staticDir+"/favicon.ico", w); err != nil {
			log.Println(err)
		}
	})

	// Handle 404
	r.NotFound(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)

		if err := serveStaticFile(staticDir+"/404.html", w); err != nil {
			log.Println(err)
		}
	})

	// Run SFU
	streamsHandler.ListenAndAccept(context.Background())

	signal.Notify(quit, os.Interrupt)

	// Configure the HTTP server
	server := &http.Server{
		Addr:              *serveAddr,
		Handler:           r,
		ReadHeaderTimeout: 1 * time.Second,
		WriteTimeout:      10 * time.Second,
	}

	// Handle shutdown
	// Register onShutdown func for close streamsHandler (notify all streams and viewers about shutdown)
	// and Websocket hub
	server.RegisterOnShutdown(func() {
		log.Println("Shutdown streams handler...")
		if err := streamsHandler.Close(); err != nil {
			log.Printf("Error during close streams handler: %v\n", err)
		}

		log.Println("Close all websocket connections...")
		if err := m.Close(); err != nil {
			log.Printf("Error during close Websocket connections: %v\n", err)
		}

		close(done)
	})

	// Shutdown the HTTP server
	go func() {
		<-quit
		log.Println("Server is going shutting down...")

		// Wait 30 seconds for close http connections
		waitIdleConnCtx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()
		server.SetKeepAlivesEnabled(false)
		if err := server.Shutdown(waitIdleConnCtx); err != nil {
			log.Fatalf("Cannot gracefully shutdown the server: %v\n", err)
		}
	}()

	// Start HTTP server
	if err := server.ListenAndServeTLS(*tlsCertFile, *tlsKeyFile); err != nil && err != http.ErrServerClosed {
		log.Fatalf("Server has been closed immediatelly: %v\n", err)
	}

	<-done
	log.Println("Server stopped")
}

func serveStaticFile(filePath string, w io.Writer) error {
	f, err := os.Open(filePath)
	if err != nil {
		return err
	}

	buf := make([]byte, 4*1024) // 4Kb
	if _, err = io.CopyBuffer(w, f, buf); err != nil {
		return err
	}

	return nil
}
