import React, { useEffect, useState } from 'react';

import Cam from './Cam.js'

import './Room.css';

const ws = new WebSocket('wss://localhost:8083/ws');
const peerConnection = new RTCPeerConnection({
  iceServers: [{
    urls: 'stun:stun.l.google.com:19302'
  }]
});

export default function Room() {
  // Stream state
  const [stream, setStream] = useState(null);
  // Start streaming button state
  const [broadcastingBtnActive, setBroadcastingBtnActive] = useState(false);
  // Streaming state
  const [broadcasting, setBroadcasting] = useState(false);

  let isOnline = false;

  useEffect(() => {
    // WebSocket
    ws.onopen = () => {
      isOnline = true;
    }

    ws.onclose = (event) => {
      // TODO: setBroadcasting(false), close peerconnection (?), show notification
      isOnline = false;
      setBroadcastingBtnActive(false);
      setStream(null);
      peerConnection.close();

      console.error("Server closed connection abnormally: ", event.code, event.reason);
      alert("Server closed connection abnormally, please reload the page");
    }

    ws.onmessage = (e) => {
      let message = JSON.parse(e.data);

      switch (message.type) {
        case 'start_stream':
          try {
            let stream = JSON.parse(message.payload);
            let sdp = JSON.parse(atob(stream.server_sdp));
            window.streamID = stream.id;

            peerConnection.setRemoteDescription(new RTCSessionDescription(sdp));
          } catch (e) {
            console.error(e);
            alert("Couldn't establish peer connection, try reload the page...");
          }
          break;
        default:
          console.error('Wrong type of message!');
      }
    };

    // PeerConnection
    peerConnection.oniceconnectionstatechange = (event) => {
      console.log('Connection state: ', peerConnection.iceConnectionState);

      if (peerConnection.iceConnectionState === 'connected') {
        setBroadcasting(true);
        setBroadcastingBtnActive(true);
      }
    };

    peerConnection.onnegotiationneeded = (event) => {
      peerConnection.createOffer().
        then((offer) => peerConnection.setLocalDescription(offer)).
        catch(console.error);
    };

    peerConnection.onicegatheringstatechange = (ev) => {
      let connection = ev.target;

      // Now we can activate broadcast button
      if (connection.iceGatheringState === 'complete') {
        let delay = 50;
        let tries = 0;
        let maxTries = 3;

        let timerId = setTimeout(function allowStreaming() {
          if (isOnline) {
            setBroadcastingBtnActive(true);
            return;
          }

          if (tries < maxTries) {
            tries += 1;
            delay *= 2;
            timerId = setTimeout(allowStreaming, delay);
          } else {
            // TODO: show user notification
            console.error("Can't connect to server");

            alert("Can't connect to server");
          }
        }, delay);
      }
    };

    navigator.mediaDevices.getUserMedia({ video: true, audio: true }).then(stream => {
      setStream(stream);

      const tracks = stream.getTracks();

      for (const track of tracks) {
        peerConnection.addTrack(track);
      }
    }).catch(console.error);

    return () => {
      ws.close();
      peerConnection.close();
    }
  }, []);

  const startStream = () => {
    setBroadcastingBtnActive(false);
    // Starting the stream...

    // Send SDP
    let message = {
      type: 'local_sdp',
      payload: btoa(JSON.stringify(peerConnection.localDescription))
    };
    ws.send(JSON.stringify(message));

    console.log('SDP has been sent...');
  };

  const stopStream = () => {
    setBroadcastingBtnActive(false);

    let message = {
      type: 'pause_stream',
      payload: window.streamID.toString()
    };
    ws.send(JSON.stringify(message));

    setBroadcasting(false);
    setBroadcastingBtnActive(true);
  };

  return (
    <div>
      <Cam stream={stream}
           broadcastingBtnActive={broadcastingBtnActive}
           broadcasting={broadcasting}
           onStartStream={startStream}
           onStopStream={stopStream}
           />
    </div>
  );
}