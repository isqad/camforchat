import React, { useRef, useEffect, useState } from "react";
import StartButton from './StartButton.js'
import StopButton from './StopButton.js'

import './Cam.css'

export default function Cam(props) {
  const videoRef = useRef(null);

  useEffect(() => {
    videoRef.current.srcObject = props.stream;
  });

  return (
    <div>
      <video width="640" height="480" autoPlay ref={videoRef} />

      {props.broadcasting ?
        <StopButton broadcastingBtnActive={props.broadcastingBtnActive} onStopStream={props.onStopStream} /> :
        <StartButton broadcastingBtnActive={props.broadcastingBtnActive} onStartStream={props.onStartStream} />}
    </div>
  );
}