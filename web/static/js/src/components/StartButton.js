export default function StartButton(props) {
  return (
    <button disabled={!props.broadcastingBtnActive}
            onClick={props.onStartStream}>
      Start stream
    </button>
  );
}