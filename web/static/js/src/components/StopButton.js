export default function StopButton(props) {
  return (
    <button disabled={!props.broadcastingBtnActive}
            onClick={props.onStopStream}>
      Stop stream
    </button>
  );
}