package cchat

import (
	"bufio"
	"cchat/internal/signal"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os/exec"
	"time"

	"github.com/google/uuid"
	"github.com/isqad/melody"

	"github.com/at-wat/ebml-go/webm"

	"github.com/pion/rtcp"
	"github.com/pion/rtp"
	"github.com/pion/rtp/codecs"
	"github.com/pion/webrtc/v2"
	"github.com/pion/webrtc/v2/pkg/media/samplebuilder"
)

// StateStream is state of the stream
type StateStream string

// Stream is object of streaming
type Stream struct {
	CreatedAt time.Time       `json:"created_at"`
	ClientSDP string          `json:"client_sdp"`
	ServerSDP string          `json:"server_sdp"`
	State     string          `json:"state"` // state of the stream
	Websocket *melody.Session `json:"-"`     // TODO: replace by NATS
	ID        uint32          `json:"id"`
	Alive     bool            `json:"-"`

	peerConfig                     webrtc.Configuration
	peerConnection                 *webrtc.PeerConnection
	pause                          chan bool
	resume                         chan bool
	audioBuilder, videoBuilder     *samplebuilder.SampleBuilder
	audioWriter, videoWriter       webm.BlockWriteCloser
	audioTimestamp, videoTimestamp uint32
}

// NewStream creates a new instance of the Stream object
func NewStream(s *melody.Session, localSDP string) *Stream {
	return &Stream{
		ID:        uuid.New().ID(), // TODO: replace by string uuid
		CreatedAt: time.Now(),
		ClientSDP: localSDP,
		Websocket: s,
		pause:     make(chan bool),
		resume:    make(chan bool),
		peerConfig: webrtc.Configuration{
			ICEServers: []webrtc.ICEServer{
				{
					URLs: iceServers,
				},
			},
		},
		audioBuilder: samplebuilder.New(20, &codecs.OpusPacket{}),
		videoBuilder: samplebuilder.New(20, &codecs.VP8Packet{}),
	}
}

// Close removes viewers and closes the stream
func (s *Stream) Close() error {
	if !s.Alive {
		return errors.New("Stream is not alive")
	}

	log.Printf("Close stream %d\n", s.ID)

	s.Alive = false
	err := s.peerConnection.Close()
	if err != nil {
		return err
	}

	return nil
}

// Pause sets to pause the stream
func (s *Stream) Pause() {
	s.pause <- true
}

// Run runs stream
func (s *Stream) Run(webrtcAPI *webrtc.API) error {
	if s.Alive {
		return errors.New("Stream already alive")
	}

	go func() {
		peerConnection, err := webrtcAPI.NewPeerConnection(s.peerConfig)
		if err != nil {
			return
		}

		s.peerConnection = peerConnection

		_, err = peerConnection.AddTransceiverFromKind(webrtc.RTPCodecTypeAudio)
		if err != nil {
			return
		}
		_, err = peerConnection.AddTransceiverFromKind(webrtc.RTPCodecTypeVideo)
		if err != nil {
			return
		}

		peerConnection.OnICEConnectionStateChange(func(connectionState webrtc.ICEConnectionState) {
			log.Printf("Stream #%d: Connection State has changed %s \n", s.ID, connectionState.String())
		})
		// Set a handler for when a new remote track starts, this handler copies inbound RTP packets,
		// replaces the SSRC and sends them back
		peerConnection.OnTrack(func(track *webrtc.Track, receiver *webrtc.RTPReceiver) {
			// Send a PLI on an interval so that the publisher is pushing a keyframe every rtcpPLIInterval
			// This is a temporary fix until we implement incoming RTCP events,
			// then we would push a PLI only when a viewer requests it
			go func() {
				ticker := time.NewTicker(time.Second * 3)
				for range ticker.C {
					errSend := peerConnection.WriteRTCP([]rtcp.Packet{&rtcp.PictureLossIndication{MediaSSRC: track.SSRC()}})
					if errSend != nil {
						fmt.Println(errSend)
					}
				}
			}()

			log.Printf("Track has been started, of type %d: %s \n", track.PayloadType(), track.Codec().Name)

			for {
				select {
				case <-s.pause:
					s.State = "paused"
					log.Println("TODO: pause stream")
				default:
					if s.State == "paused" {
						continue
					}

					// Read RTP packets being sent to Pion
					rtp, err := track.ReadRTP()
					if err != nil {
						log.Printf("Reading error: %v\n", err)
					}

					switch track.Kind() {
					case webrtc.RTPCodecTypeAudio:
						s.pushOpus(rtp)
					case webrtc.RTPCodecTypeVideo:
						s.pushVP8(rtp)
					}
				}
			}
		})

		offer := webrtc.SessionDescription{}
		signal.Decode(s.ClientSDP, &offer)
		// Set the remote SessionDescription
		err = peerConnection.SetRemoteDescription(offer)
		if err != nil {
			return
		}

		// Create an answer
		answer, err := peerConnection.CreateAnswer(nil)
		if err != nil {
			return
		}
		s.ServerSDP = signal.Encode(answer)

		// Sets the LocalDescription, and starts our UDP listeners
		err = peerConnection.SetLocalDescription(answer)
		if err != nil {
			return
		}

		streamJSON, err := json.Marshal(s)
		if err != nil {
			log.Printf("marshaling Stream failed: %v\n", err)
			return
		}

		message, err := json.Marshal(&WsEvent{
			Type:    StartStreamEvent,
			Payload: string(streamJSON[:]),
		})
		if err != nil {
			log.Printf("marshaling SDP answer failed: %v\n", err)
			return
		}

		s.Alive = true

		s.Websocket.Write(message)

		log.Printf("Stream #%d has been started!\n", s.ID)
		for {
		}
	}()

	return nil
}

func (s *Stream) broadcast(width, height int) {
	streamKey := ``
	// Create a ffmpeg process that consumes MKV via stdin, and broadcasts out to Twitch
	ffmpeg := exec.Command("ffmpeg", "-re", "-i", "pipe:0", "-c:v", "libx264", "-preset", "veryfast", "-maxrate", "3000k", "-bufsize", "6000k", "-pix_fmt", "yuv420p", "-g", "50", "-c:a", "aac", "-b:a", "160k", "-ac", "2", "-ar", "44100", "-f", "flv", fmt.Sprintf("rtmp://live.twitch.tv/app/%s", streamKey)) //nolint
	ffmpegIn, _ := ffmpeg.StdinPipe()
	ffmpegOut, _ := ffmpeg.StderrPipe()
	if err := ffmpeg.Start(); err != nil {
		panic(err)
	}

	go func() {
		scanner := bufio.NewScanner(ffmpegOut)
		for scanner.Scan() {
			fmt.Println(scanner.Text())
		}
	}()

	ws, err := webm.NewSimpleBlockWriter(ffmpegIn,
		[]webm.TrackEntry{
			{
				Name:            "Audio",
				TrackNumber:     1,
				TrackUID:        12345,
				CodecID:         "A_OPUS",
				TrackType:       2,
				DefaultDuration: 20000000,
				Audio: &webm.Audio{
					SamplingFrequency: 48000.0,
					Channels:          2,
				},
			}, {
				Name:            "Video",
				TrackNumber:     2,
				TrackUID:        67890,
				CodecID:         "V_VP8",
				TrackType:       1,
				DefaultDuration: 33333333,
				Video: &webm.Video{
					PixelWidth:  uint64(width),
					PixelHeight: uint64(height),
				},
			},
		})
	if err != nil {
		panic(err)
	}

	fmt.Printf("WebM saver has started with video width=%d, height=%d\n", width, height)
	s.audioWriter = ws[0]
	s.videoWriter = ws[1]
}

// Parse Opus audio and Write to WebM
func (s *Stream) pushOpus(rtpPacket *rtp.Packet) {
	s.audioBuilder.Push(rtpPacket)

	for {
		sample := s.audioBuilder.Pop()
		if sample == nil {
			return
		}
		if s.audioWriter != nil {
			s.audioTimestamp += sample.Samples
			t := s.audioTimestamp / 48
			if _, err := s.audioWriter.Write(true, int64(t), rtpPacket.Payload); err != nil {
				panic(err)
			}
		}
	}
}

// Parse VP8 video and Write to WebM
func (s *Stream) pushVP8(rtpPacket *rtp.Packet) {
	s.videoBuilder.Push(rtpPacket)

	for {
		sample := s.videoBuilder.Pop()
		if sample == nil {
			return
		}
		// Read VP8 header.
		videoKeyframe := (sample.Data[0]&0x1 == 0)
		if videoKeyframe {
			// Keyframe has frame information.
			raw := uint(sample.Data[6]) | uint(sample.Data[7])<<8 | uint(sample.Data[8])<<16 | uint(sample.Data[9])<<24
			width := int(raw & 0x3FFF)
			height := int((raw >> 16) & 0x3FFF)

			if s.videoWriter == nil || s.audioWriter == nil {
				// Initialize WebM saver using received frame size.
				s.broadcast(width, height)
			}
		}
		if s.videoWriter != nil {
			s.videoTimestamp += sample.Samples
			t := s.videoTimestamp / 90
			if _, err := s.videoWriter.Write(videoKeyframe, int64(t), sample.Data); err != nil {
				panic(err)
			}
		}
	}
}
